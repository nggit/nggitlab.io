# visits
visits is just an example of a web page to display hits.

## How-to
```
python3 -m visits [PORT_NUMBER]
```

## License
MIT
